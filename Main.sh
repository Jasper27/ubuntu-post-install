#!/bin/bash





username="$USER"
#echo $username

cat << "EOF"


     _                           _       _     _                             _               
    | | __ _ ___ _ __   ___ _ __( )___  | |   (_)_ __  _   ___  __  ___  ___| |_ _   _ _ __  
 _  | |/ _` / __| '_ \ / _ \ '__|// __| | |   | | '_ \| | | \ \/ / / __|/ _ \ __| | | | '_ \ 
| |_| | (_| \__ \ |_) |  __/ |    \__ \ | |___| | | | | |_| |>  <  \__ \  __/ |_| |_| | |_) |
 \___/ \__,_|___/ .__/ \___|_|    |___/ |_____|_|_| |_|\__,_/_/\_\ |___/\___|\__|\__,_| .__/ 
                |_|                                                                   |_|    
										
										By Jasper27
						
EOF


#Checks if the user is root 

if [ $EUID = 0 ]; then
  	echo "Please only run as root if you want to configure the root account"
 	echo "Enter 'YES' to continue"
 	read runAsRoot
 	
 	#If user is root
 	if [ $runAsRoot == "YES" ]
 		then
 		echo "If you say so... :-)"
 		echo ""
 	else 
 		exit 
 	fi
fi



echo "auto	Choose sane settings"
echo "1)	install apt packages"
echo "2)	Setup snapd"
echo "4)	setup flatpak"
echo "4) 	Setup firewall"
echo "5) 	Move Backgrounds"
echo "6) 	Move scripts"
echo "7) 	Swap config files"
echo "8) 	Change to fish"
echo "9)	Remove bloat from apt"


echo ""
echo -n "Select modules (e.g. 1 8 ):  " && read choices

sudo echo "You are gunna need sudo for this"


if [[ "$choices" == *"1"* ]]; then
	aptWant=true 
	echo hello
fi

if [[ "$choices" == *"2"* ]]; then
	snapdwant=true 
fi

if [[ "$choices" == *"3"* ]]; then
	flatwant=true 
fi

if [[ "$choices" == *"4"* ]]; then
	firewallWant=true 
fi

if [[ "$choices" == *"5"* ]]; then
	backWant=true 
fi

if [[ "$choices" == *"6"* ]]; then
	scriptWant=true 
fi

if [[ "$choices" == *"7"* ]]; then
	configWant=true 
fi

if [[ "$choices" == *"8"* ]]; then
	fishWant=true 
fi

if [[ "$choices" == *"9"* ]]; then
	aptBloat=true 
fi


if [[ "$choices" == "" || "$choices" == *"auto"* ]]; then
	aptWant=true 
	snapdwant=false 
	flatwant=true 
	firewallWant=true 
	backWant=true  
	scriptWant=true 
	configWant=true
	fishwant=true
	aptBloat=true 
fi



#Asking user what they want 





###Flatpak 

if [ "$flatwant" == true ]; then
	echo "Setting up flatpak :-)" 
        
	sudo apt install flatpak
	sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	sudo flatpak install flathub $(< resources/flatPackages.txt) -y 
	sudo flatpak update
	echo ""
fi



if [ "$snapdwant" == true ]; then
	echo "Setting up snapd :-)" 
        
	sudo apt install snapd
	sudo snap refresh 
	sudo snap install $(< resources/snapPackages.txt) -y 
	sudo snap refresh
	echo ""
fi



if [ "$aptWant" == true ]; then
	echo "Intall packages from package list"

	# Install packages 
	sudo apt install $(< resources/packages.txt) -y
	echo ""
fi

if [ "$aptBloat" == true ]; then
	echo "Removing packages from bloat list"

	# Install packages 
	sudo apt-get --purge remove $(< resources/aptBloat.txt) -y
	echo ""
fi

# updates and things 
sudo apt update -y
sudo apt upgrade -y 
sudo apt autoremove -y 
echo ""

if [ "$firewallWant" == true ]; then
	echo "Setting up firewall"
	# firewall
	sudo systemctl start ufw.service
	sudo systemctl enable ufw.service
	sudo ufw enable

	sudo ufw default deny incoming
	sudo ufw default allow outgoing

	echo -n "Firewall" && sudo ufw status

	echo ""
fi



if [ "$backWant" == true ]; then
	echo "Adding backgrounds to /usr/share/backgrounds"
	### Add the backgrounds
	sudo cp -Rv resources/backgrounds/* /usr/share/backgrounds
	echo ""

fi



if [ "$scriptWant" == true ]; then
	echo "Adding scripts to /usr/bin"
	### Add scripts to usr/bin/ to make custom commands 
	sudo cp -Rv resources/scripts/* /usr/bin 
	echo""

fi



if [ "$configWant" == true ]; then
	echo "Swapping out config files"
	###Swapping out config files 
	##bashrc
	mv -v ~/.bashrc ~/.bashrc.bak
	cp -v resources/configs/bashrc ~/.bashrc
	##

	##zshrc
	mv -v ~/.zshrc ~/.zshrc.bak
	cp -v resources/configs/zshrc ~/.zshrc
	##

	##fish 
	mv -v ~/.config/fish ~/.config/fish.old
	cp -avr resources/configs/fish ~/.config/fish
	#echo /usr/local/bin/fish | sudo tee -a /etc/shells
	echo ""

fi



if [ "$fishWant" == true ]; then
	echo "Changing the users shell to FISH"

	#Changes users default shell to fish
	sudo usermod -s /usr/bin/fish "$username"
	echo ""
	

fi



###Just a tidy up to optimise stuff

sudo apt-get autoclean -y 
sudo apt-get autoremove -y 
sudo apt-get clean -y 
echo ""




cat << "EOF"
     _    _ _       _                         __
    / \  | | |   __| | ___  _ __   ___   _    \ \
   / _ \ | | |  / _` |/ _ \| '_ \ / _ \ (_)____| |
  / ___ \| | | | (_| | (_) | | | |  __/  |_____| |
 /_/   \_\_|_|  \__,_|\___/|_| |_|\___| (_)    | |
                                              /_/
EOF

